#!/bin/bash
# Execute script with doas for validation of doas config over sudo

echo -e "\033[0;38m-------------------------------------------------------------------------------------------\033[0m"
echo -e "\033[1;31mplagueOS Settings Audit \033[1;31m"
echo -e "\033[0;38m-------------------------------------------------------------------------------------------\033[0m"

echo "Generic host name: "
echo -e "\033[0;38m-------------------------------------------------------------------------------------------\033[0m"
cat /etc/hosts
echo " "

echo "Validating OpenNTPD configuration: "
echo -e "\033[0;38m-------------------------------------------------------------------------------------------\033[0m"
openntpd -n # validate configuration file
date
echo " "

echo "Randomized Mac"
echo -e "\033[0;38m-------------------------------------------------------------------------------------------\033[0m"
ip a | grep "permaddr"

echo "Presence of hardened_malloc: "
echo -e "\033[0;38m-------------------------------------------------------------------------------------------\033[0m"
ls /usr/local/lib/hardened_malloc.so
echo " "

echo "List sysctl configuration files: "
echo -e "\033[0;38m-------------------------------------------------------------------------------------------\033[0m"
ls -la /etc/sysctl.d/

echo "Display sysctl configuration: "
echo -e "\033[0;38m-------------------------------------------------------------------------------------------\033[0m"
cat /etc/sysctl.d/10*
echo " "
cat /etc/sysctl.d/20*
echo " "
cat /etc/sysctl.d/30*
echo " "
cat /etc/sysctl.d/40*
echo " "
cat /etc/sysctl.d/50*
echo " "
cat /etc/sysctl.d/60*
echo " "
cat /etc/sysctl.d/70*

echo " "
echo " "
echo " "

echo "/etc/modprobe.d/ configuration: "
echo -e "\033[0;38m-------------------------------------------------------------------------------------------\033[0m"
echo " "
cat /etc/modprobe.d/blacklist.conf 
echo " "
echo " "
echo " "

echo "IPTables set?"
echo -e "\033[0;38m-------------------------------------------------------------------------------------------\033[0m"
iptables -L -n
echo " "
echo " "
echo " "

echo "Root and admin user status: "
echo -e "\033[0;38m-------------------------------------------------------------------------------------------\033[0m"
passwd -S root
passwd -S admin
echo " "
echo " "
echo " "

echo "Present /plague/bin files"
echo -e "\033[0;38m-------------------------------------------------------------------------------------------\033[0m"
ls -la /plague/bin/
echo " "
echo " "
echo " "

echo "Core dumps disabled?"
echo -e "\033[0;38m-------------------------------------------------------------------------------------------\033[0m"
cat /etc/security/limits.conf | grep "hard core"
echo " "
echo " "
echo " "

echo "Validate UMASK is set"
echo -e "\033[0;38m-------------------------------------------------------------------------------------------\033[0m"
cat /etc/profile | grep "umask 0"
echo " "
echo " "
echo " "

echo "Check machine ID (should be generic 'b08dfa6083e7567a1921a715000001fb')"
echo -e "\033[0;38m-------------------------------------------------------------------------------------------\033[0m"
cat /etc/machine-id 
echo " "
echo " "
echo " "

echo "user enumeration: "
echo -e "\033[0;38m-------------------------------------------------------------------------------------------\033[0m"
cat /etc/passwd
echo " "
echo " "
echo " "
 
echo "/etc/fstab configuration: "
echo -e "\033[0;38m-------------------------------------------------------------------------------------------\033[0m"
cat /etc/fstab
echo " "
echo " "
echo " "
echo "hardened boot parameters: "
echo -e "\033[0;38m-------------------------------------------------------------------------------------------\033[0m"
cat /etc/default/grub | grep CMDLINE_LINUX_DEFAULT=
echo " "
echo " "
echo " "

echo "Check directory permissions"
echo -e "\033[0;38m-------------------------------------------------------------------------------------------\033[0m"
ls -ld /boot
ls -ld /etc/iptables
ls -ld /usr/src
ls -ld /lib/modules
ls -ld /usr/lib/modules
ls -ld /home/admin
echo " "
echo " "
echo " "
echo "Audit complete"
exit