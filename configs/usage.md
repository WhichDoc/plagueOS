# About PlagueOS
PlagueOS is not associated with the TOR project or with Whonix.
PlagueOS has received funds by open-source enthusiasts from the Pirate Chain (ARRR) community, however we have no direct affiliations with the project. 
This is not developed by an organization; this is a passion project created and maintained by a few Linux enthusiasts. 
This OS is not resistant to all forms of exploitation; there are shortcomings inherent to the modern desktop environment that we are attempting to mitigate. 
The OS is designed to run KVM as a minimalist hypervisor with security-centric virtual machines (guests) from `virt-manager`.
Whonix and Kicksecure can be imported and validated during the installation script. Any other guests will be a manual endeavor. 

# Proper Usage
There are two users by default: `admin` and `user`.
All updates, upgrades, and package installations (perhaps KeepassXC or ZuluCrypt) must be installed by the `admin` account.
All daily activities are to be conducted by the unprivileged `user` account. The `user` account has permissions to access `virt-manager` and the related guests.

## File and Folder Permissions
By default, PlagueOS has a strong permission set.
This means that newly created files will be unaccessible by your `user` account, or potentially by other applications.
You can check the permissions of 'files' and 'folders' by navigating to the directory that they are located in, by typing `ls -la` inside the terminal / console.
The `ls` command above is going to list permissions for every file or folder that's inside the directory you are in.
The output you get means: `r` = read, `w` = write, `x` = execute.
To change the permissions of either files or folders, type the `chmod` command inside the terminal, as `admin`: --> `chmod <value> <file/folder>`
There are plenty of sites that can help you to understand how the 'chmod' command works, by automating what you wish to do. Search `Chmod calculator` with your preferred search engine.

## Mounting USB Devices
`USBGuard` is present on the installation to prevent auto-mounting of USB Drives, as they are a commonly known attack vector. To allow a USB device to be mounted, you must run `doas usbguard list-devices` and `doas usbguard allow-device <number>` from the `admin` account. To permanently save the rule, append a `-p` to the `usbguard allow-device <number>` command.
In the event that issues occur with USBGuard, run `sudo su` to login to the root user. Execute the previously listed commands with `usbguard-daemon` rather than `usbguard`.

## Traffic Routing
By default, the packages `openvpn` and `wireguard` are not present on the host. If a VPN is desired, these packages or custom wrappers from the provider must be installed. 
'TOR' can be configured during install to be leveraged for all package updates and installations. NOTE: Not all traffic is routed through 'TOR', and software-based routing solutions are not impervious to leakage.

# Limitations
This OS cannot mitigate the actions which are caused by user error. If you are acting outside of the bounds of [proper usage](#proper-usage), this is outside the scope of our mitigations.

# Baby Steps
Do note that this project is still in its infancy, and bugs across different classes of hardware are likely to occur. We will help with bugs the best that we can, however we cannot investigate an error that we cannot reproduce or visualize. We appreciate any assistance with bug reporting.

# Development
All contributions to the PlagueOS project are welcome. This could include actively testing exploitation mechanisms against the OS from VM escapes to privilege escalation from the user account, pushing or providing steps to remove a bug, steps to include to implement one of the listed feature requests, or suggesting / porting over features that can increase the overall security of PlagueOS.
